# PHEME - DESKTOP

Created with the following technologies:

 * HTML
 * CSS
 * JavaScript/jQuery
 * MDL - Material Designs Lite

You can see the screenshots which I've taken [on this](https://github.com/anonymousguyx/PHEME-Desktop/tree/master/screenshots) page. 


If you see any flaw, please let me know or do a commit/comment. I'll modify it.
This is an ongoing project, I'll complete it soon.

### Thank you!
